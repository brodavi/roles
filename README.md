Roles can be played by multiple people over the course of a project, but the core responsibilities should be carried out.
