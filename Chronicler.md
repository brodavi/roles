# Chronicler

## Intention

- To maintain a concise and accurate record of a project's progress, successes, and challenges.
- To free up other roles to discuss and strategize freely by keeping track of and distributing action items.

## Work Supports

- Project retrospectives.
- Case studies.
- Progress updates for clients.
- Onboarding new collaborators.

## Responsibilities

The Chronicler will:

- Provide detailed notes of the meeting, recorded while the meeting is in progress.
- Provide a brief (2-3 sentence) summary of the meeting or event.
- Provide a list of action items.
- Ensure all action items are properly delegated.
- Collect and tag any artifacts produced from the meeting.
- Ensure the event is properly logged in the Lab.

## Guidelines

The Chronicler:

- Will have no additional responsibilities.
- Will not be expected to contribute in the meeting itself.
