# Facilitator

## Responsibilities
- Confirms that the meeting has a clear intention and that each member is satisfied that the intention has been met.
- Keeps meetings on-track by keeping track of time.
